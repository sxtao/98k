import { Mat3, Quat, Vec3 } from "cc";
import { Dirty, cObject } from "./Object";
import { cShape } from "./Shape";


export class cBody {

    id: number = 0;
    mask: number = 0;
    group: number = 0;
    shape: cShape = null;
    object: cObject = null;

    //脏区更新标记
    isDirty: number = 1 | 2 | 4;

    //缓存
    lower: number = 0;
    upper: number = 0;
    aabb: Array<number> = [0, 0, 0, 0, 0, 0];

    //状态
    isRemove: boolean = false;
    isRetrieve: boolean = true;
    isIdentity: boolean = true;
    inCollider: boolean = false;

    //缓存
    raidus: number = 0;
    center: Vec3 = new Vec3();
    rotMat3: Mat3 = new Mat3();
    halfSize: Vec3 = new Vec3();

    constructor(obj: cObject) {
        this.object = obj;
    }
    

    updateBound(isDirty: Dirty = Dirty.NON) {

        let object = this.object;
        
        object.hasChangeDirty();
        isDirty|=object.isDirty;

        if (isDirty > 0) {

            let aabbChange = false;
            const shape = this.shape;
            
            if(isDirty & Dirty.S){
                aabbChange = true;
                shape.setScale(this.getScale());
            }

            if (isDirty & Dirty.R) {
                //旋转更新aabb
                this.isIdentity = false;
                let rot = this.getRotation();
                this.rotMat3.fromQuat(rot); //计算缓存Mat3

                if (rot.equals(Quat.IDENTITY, 0.0001)) {
                    this.isIdentity = true;
                }

                aabbChange = true;
            }

            if(aabbChange) shape.updateAABB(this.rotMat3, this.isIdentity);

            object.isDirty = Dirty.NON;


            const AABB = this.aabb;// world aabb
            const aabb = shape.aabb; //local aabb
            const p = this.getPosition(); //world postion

            AABB[0] = aabb[0] + p.x;
            AABB[1] = aabb[1] + p.y;
            AABB[2] = aabb[2] + p.z;

            AABB[3] = aabb[3] + p.x;
            AABB[4] = aabb[4] + p.y;
            AABB[5] = aabb[5] + p.z;

            this.isDirty = 1 | 2 | 4; //设置脏区标记

            return true;
        }

        return false;
    }

    clear() {

        // this.id = 0;
        this.shape = null;
        this.object.body = null;
        this.object = null;
        this.inCollider = false;
        this.isRemove = false;

    }


    //body 坐标统一使用世界数据进行计算
    getScale() { return this.object.node.worldScale; } // world scale 
    getPosition() { return this.object.node.worldPosition; } //wold position
    getRotation() { return this.object.node.worldRotation; } //world rotation


    getRotMat3() {
        //world rotate mat3
        return this.rotMat3;
    }

    getCenter() {


        if (this.isDirty & 1) {
            this.isDirty &= (~1);

            const aabb = this.aabb;
            const center = this.center;
            center.x = (aabb[0] + aabb[3]) * 0.5;
            center.y = (aabb[1] + aabb[4]) * 0.5;
            center.z = (aabb[2] + aabb[5]) * 0.5;
        }

        return this.center; //world center
    }

    getRaidus() {


        if (this.isDirty & 2) {
            this.isDirty &= (~2);

            const s = this.shape.scale;
            const raidus = this.shape.radius;
            const scale = Math.max(s.x, s.y, s.z);
            this.raidus = scale * raidus;

        }

        return this.raidus; //world raidus
    }

    getHalfSize() {


        if (this.isDirty & 4) {
            this.isDirty &= (~4);

            const s = this.shape.scale;
            const world = this.halfSize;
            const local = this.shape.halfSize;

            world.x = s.x * local.x;
            world.y = s.y * local.y;
            world.z = s.z * local.z;

        }

        return this.halfSize; //world halfsize
    }
}
